from pathlib import Path

from sila2_example_server import Client


def main():
    certificate_authority = Path("ca.pem").read_bytes()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # get current value
    start_year = client.GreetingProvider.StartYear.get()
    print(f"Server was started in the year {start_year}")


if __name__ == "__main__":
    main()
