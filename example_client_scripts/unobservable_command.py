from pathlib import Path

from sila2_example_server import Client


def main():
    certificate_authority = Path("ca.pem").read_bytes()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # command parameters can be specified as positional or keyword arguments:
    response1 = client.GreetingProvider.SayHello("World")
    response2 = client.GreetingProvider.SayHello(Name="World")
    assert response1 == response2

    # commands can have multiple responses, so they return a NamedTuple with one field per response.
    # the command GreetingProvider.SayHello has one response called 'Greeting':
    greeting1 = response1.Greeting

    # iterable unpacking also works:
    (greeting2,) = response2
    assert greeting1 == greeting2

    print(f"Server says '{greeting1}'")


if __name__ == "__main__":
    main()
