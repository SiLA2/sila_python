# Configuration file for the Sphinx documentation builder
import sys

import sila2

project = sila2.__name__
author = "Niklas Mertsch"
copyright = f"2022, {author}"  # noqa: A001, shadows built-in `copyright`
release = sila2.__version__
language = "en"

exclude_patterns = ["_build"]

html_theme = "furo"
html_static_path = ["_static"]
html_favicon = "_static/favicon.ico"

pygments_style = "sphinx"

autoclass_content = "both"
autodoc_default_options = {
    "inherited-members": True,  # when set to False, some non-inherited members won't be displayed (no clue why...)
    "exclude-members": ", ".join(  # noqa: FLY002, use string literal instead (less readable here)
        [
            # (named) tuples
            "count",
            "index",
            # exceptions
            "with_traceback",
        ]
    ),
}

sys.path.append("extensions")
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinxcontrib.runcmd",
    "intersphinx_typing_fix",
]
autodoc_member_order = "bysource"
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
}
napoleon_use_rtype = False
