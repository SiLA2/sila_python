#!/usr/bin/env python
# os-agnostic replacement for sphinx Makefile
import os
import shutil
import sys
from os.path import abspath, dirname, join

docs_dir = abspath(dirname(__file__))
build_dir = join(docs_dir, "_build")
html_dir = join(docs_dir, "_build", "html")

os.chdir(docs_dir)  # required by sphinx-build

args = " ".join(sys.argv[1:])
if not args:
    args = "html"

if args not in ["html", "clean", "clean html"]:
    raise ValueError("args must be clean and/or html (default: html)")

if "clean" in args and os.path.exists(build_dir):
    shutil.rmtree(build_dir)
if "html" in args:
    # -W turn warnings into errors
    # -T show full traceback on exception
    # -b builder to use (e.g. `html`)
    exit_code = os.system(f"{sys.executable} -m sphinx -W -T -b html {docs_dir} {html_dir}")  # noqa: S605
    sys.exit(exit_code)
