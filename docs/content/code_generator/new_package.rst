``sila2-codegen new-package``: Generate new SiLA 2 Server/Client package
========================================================================

Use this command to generate a full SiLA application.

.. runcmd:: sila2-codegen new-package --help

.. note::

    You do not have to provide any features when generating the package.
    You can always add more features by using the :doc:`sila2-codegen add-features <add_features>` command.
