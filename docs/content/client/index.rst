SiLA Client usage
=================

The class :py:class:`sila2.client.SilaClient` represents SiLA Clients and can be used to connect to and interact with SiLA Servers.

All code example in this section use the `example SiLA 2 Server <https://gitlab.com/SiLA2/sila_python/-/tree/master/example_server>`_
from the `GitLab repository <https://gitlab.com/SiLA2/sila_python>`_:

.. code-block:: python

    >>> from sila2_example_server import Client

    >>> client = Client(...)

You can find more SiLA Client examples `here <https://gitlab.com/SiLA2/sila_python/-/tree/master/example_client_scripts>`_.

.. toctree::

    connection
    usage
    code_generator
