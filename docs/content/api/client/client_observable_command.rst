Class ClientObservableCommand
=============================

.. autoclass:: sila2.client.ClientObservableCommand
    :special-members: __call__

Class ClientObservableCommandInstance
=====================================

.. autoclass:: sila2.client.ClientObservableCommandInstance

.. autoclass:: sila2.client.ClientObservableCommandInstanceWithIntermediateResponses
    :show-inheritance:
    :no-inherited-members:
    :members: subscribe_to_intermediate_responses, get_intermediate_response
