#!/usr/bin/env python
import os
import stat
import sys
from os.path import dirname, join

hook_file = join(dirname(__file__), ".git", "hooks", "pre-commit")

# short-running tests
pytest_target_files = " ".join(  # noqa: FLY002, use string literal instead (less readable here)
    [
        "tests/framework/",
        "tests/end_to_end/test_unobservable_feature_impl.py",
        "tests/end_to_end/test_binary_transfer.py",
        "tests/end_to_end/test_silaservice.py",
        "tests/end_to_end/test_error_handling.py",
    ]
)

# write pre-commit hook script to .git/hooks/pre-commit
with open(hook_file, "w", encoding="utf-8") as fp:
    fp.write(
        f"""\
#!/bin/sh
set -ex

# ensure correct environment is used (workaround for PyCharm)
alias python={sys.executable}

python -m ruff check --select I.
python -m ruff format --check .
python -m pytest {pytest_target_files}
"""
    )

# make script executable
os.chmod(hook_file, os.stat(hook_file).st_mode | stat.S_IEXEC)
