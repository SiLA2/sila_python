import pytest

from sila2.framework.data_types.integer import Integer
from sila2.framework.feature import Feature
from sila2.framework.property.unobservable_property import UnobservableProperty
from tests.utils import create_server_stub, get_feature_definition_str


@pytest.fixture
def unobservable_property_feature() -> Feature:
    return Feature(get_feature_definition_str("UnobservableProperty"))


def test_attributes(unobservable_property_feature):
    prop = unobservable_property_feature._unobservable_properties["TestProperty"]

    assert isinstance(prop, UnobservableProperty)
    assert prop._identifier == "TestProperty"
    assert prop._display_name == "Test Property"
    assert prop._description == "An unobservable property"
    assert prop.fully_qualified_identifier == "de.unigoettingen/tests/UnobservableProperty/v1/Property/TestProperty"

    assert prop.parameter_message_type.__name__ == "Get_TestProperty_Parameters"
    assert prop.response_message_type.__name__ == "Get_TestProperty_Responses"

    assert isinstance(prop.data_type, Integer)


def test_grpc(unobservable_property_feature):
    prop = unobservable_property_feature._unobservable_properties["TestProperty"]

    class UnobservablePropertyImpl(unobservable_property_feature._servicer_cls):
        def Get_TestProperty(self, request, context):
            return prop.to_message(3)

    server, stub = create_server_stub(unobservable_property_feature._grpc_module, UnobservablePropertyImpl())

    assert prop.to_native_type(stub.Get_TestProperty(prop.get_parameters_message())) == 3
