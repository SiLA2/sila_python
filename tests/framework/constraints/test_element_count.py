from sila2.framework.constraints.element_count import ElementCount


def test():
    c = ElementCount(3)

    assert not c.validate([1, 2])
    assert c.validate([1, 2, 3])
    assert not c.validate([1, 2, 3, 4])

    assert repr(c) == "ElementCount(3)"
