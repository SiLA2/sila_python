from sila2.framework.constraints.minimal_inclusive import MinimalInclusive
from sila2.framework.data_types.integer import Integer
from sila2.framework.data_types.real import Real


def test(silaframework_pb2_module):
    m_int = MinimalInclusive("3", Integer())
    assert not m_int.validate(2)
    assert m_int.validate(3)
    assert m_int.validate(4)

    assert repr(m_int) == "MinimalInclusive(3.0)"

    m_float = MinimalInclusive("2.5", Real())
    assert not m_float.validate(2.4)
    assert m_float.validate(2.5)
    assert m_float.validate(3)


def test_scientific_notation(silaframework_pb2_module):
    m_int = MinimalInclusive("3e5", base_type=Integer())
    m_float = MinimalInclusive("1.23e-4", base_type=Real())

    assert m_int.validate(300_000)
    assert not m_int.validate(299_999)

    assert m_float.validate(0.000123)
    assert not m_float.validate(0.0001229)
