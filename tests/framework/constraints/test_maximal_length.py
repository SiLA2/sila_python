import pytest

from sila2.framework.constraints.maximal_length import MaximalLength


def test():
    length3 = MaximalLength(3)
    assert length3.validate("abc")
    assert length3.validate(b"abc")
    assert length3.validate("ab")
    assert not length3.validate("abcd")

    assert repr(length3) == "MaximalLength(3)"

    _ = MaximalLength(2**63 - 1)
    with pytest.raises(ValueError, match="Length cannot be negative"):
        _ = MaximalLength(-1)
    with pytest.raises(ValueError, match="Length cannot be greater than"):
        _ = MaximalLength(2**63)
