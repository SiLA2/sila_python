from sila2.framework.data_types.data_type_definition import DataTypeDefinition
from sila2.framework.data_types.integer import Integer


def test(basic_feature):
    dtype = basic_feature._data_type_definitions["Int"]
    assert isinstance(dtype, DataTypeDefinition)
    assert dtype._identifier == "Int"
    assert dtype._display_name == "Int"
    assert dtype._description == "An integer"
    assert dtype.fully_qualified_identifier == "de.unigoettingen/tests/BasicDataTypes/v1/DataType/Int"
    assert isinstance(dtype.data_type, Integer)

    msg = dtype.to_message(1)
    assert msg.__class__.__name__ == "DataType_Int"
    assert msg.Int.value == 1
    assert dtype.to_native_type(msg) == 1
