import pytest

from sila2.framework.data_types.boolean import Boolean
from sila2.framework.pb2 import SiLAFramework_pb2


def test_to_native_type():
    msg = SiLAFramework_pb2.Boolean(value=False)
    val = Boolean().to_native_type(msg)

    assert isinstance(val, bool)
    assert not val


def test_to_message():
    msg = Boolean().to_message(True)
    assert msg.value is True


def test_wrong_type():
    boolean_field = Boolean()

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        boolean_field.to_message(1)
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        boolean_field.to_message("")
