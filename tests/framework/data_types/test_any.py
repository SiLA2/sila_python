import typing
from datetime import date, datetime, time, timedelta, timezone
from typing import NamedTuple

import pytest

from sila2.framework import SilaDateType
from sila2.framework.data_types.any import Any, SilaAnyType


class AnyTestValue(NamedTuple):
    name: str
    type_xml: str
    python_value: typing.Any


TEST_VALUES = [
    AnyTestValue(
        "Basic string",
        "<DataType><Basic>String</Basic></DataType>",
        "abc",
    ),
    AnyTestValue(
        "Basic integer",
        "<DataType><Basic>Integer</Basic></DataType>",
        123,
    ),
    AnyTestValue(
        "Basic real",
        "<DataType><Basic>Real</Basic></DataType>",
        1.234,
    ),
    AnyTestValue(
        "Basic binary",
        "<DataType><Basic>Binary</Basic></DataType>",
        b"abc",
    ),
    AnyTestValue(
        "Basic date",
        "<DataType><Basic>Date</Basic></DataType>",
        SilaDateType(date(2020, 12, 24), timezone(timedelta(hours=-2))),
    ),
    AnyTestValue(
        "Basic time",
        "<DataType><Basic>Time</Basic></DataType>",
        time(13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20))),
    ),
    AnyTestValue(
        "Basic timestamp",
        "<DataType><Basic>Timestamp</Basic></DataType>",
        datetime(2020, 12, 24, 13, 59, 26, tzinfo=timezone(timedelta(hours=3, minutes=20))),
    ),
    AnyTestValue(
        "Void",
        """<DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Length>0</Length>
                </Constraints>
            </Constrained>
        </DataType>""",
        None,
    ),
    AnyTestValue(
        "Constrained string",
        """<DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Length>3</Length>
                </Constraints>
            </Constrained>
        </DataType>""",
        "abc",
    ),
    AnyTestValue(
        "Basic list",
        """
        <DataType>
            <List>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
            </List>
        </DataType>
        """,
        ["abc", "def", "ghi"],
    ),
    AnyTestValue(
        "Constrained basic list",
        """
        <DataType>
            <Constrained>
                <DataType>
                    <List>
                        <DataType>
                            <Basic>String</Basic>
                        </DataType>
                    </List>
                </DataType>
                <Constraints>
                    <ElementCount>3</ElementCount>
                </Constraints>
            </Constrained>
        </DataType>
        """,
        ["abc", "def", "ghi"],
    ),
    AnyTestValue(
        "Structure",
        """
        <DataType>
            <Structure>
                <Element>
                    <Identifier>Element1</Identifier>
                    <DisplayName>Element1</DisplayName>
                    <Description>First element</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element2</Identifier>
                    <DisplayName>Element2</DisplayName>
                    <Description>Second element</Description>
                    <DataType>
                        <Structure>
                            <Element>
                                <Identifier>Subelement1</Identifier>
                                <DisplayName>Subelement1</DisplayName>
                                <Description>First subelement</Description>
                                <DataType>
                                    <Basic>Boolean</Basic>
                                </DataType>
                            </Element>
                            <Element>
                                <Identifier>Subelement2</Identifier>
                                <DisplayName>Subelement2</DisplayName>
                                <Description>Second subelement</Description>
                                <DataType>
                                    <Basic>Binary</Basic>
                                </DataType>
                            </Element>
                        </Structure>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element3</Identifier>
                    <DisplayName>Element3</DisplayName>
                    <Description>Third element</Description>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
        """,
        (1, (True, b"abc"), "def"),
    ),
    AnyTestValue(
        "Structure with list",
        """
        <DataType>
            <Structure>
                <Element>
                    <Identifier>Element1</Identifier>
                    <DisplayName>Element1</DisplayName>
                    <Description>First element</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Element2</Identifier>
                    <DisplayName>Element2</DisplayName>
                    <Description>Second element</Description>
                    <DataType>
                        <List>
                            <DataType>
                                <Basic>String</Basic>
                            </DataType>
                        </List>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
        """,
        (12, ["ab", "c", "de"]),
    ),
    AnyTestValue(
        "List with structure",
        """
        <DataType>
            <List>
                <DataType>
                    <Structure>
                        <Element>
                            <Identifier>Element1</Identifier>
                            <DisplayName>Element1</DisplayName>
                            <Description>First element</Description>
                            <DataType>
                                <Basic>Integer</Basic>
                            </DataType>
                        </Element>
                        <Element>
                            <Identifier>Element2</Identifier>
                            <DisplayName>Element2</DisplayName>
                            <Description>Second element</Description>
                            <DataType>
                                <List>
                                    <DataType>
                                        <Basic>String</Basic>
                                    </DataType>
                                </List>
                            </DataType>
                        </Element>
                    </Structure>
                </DataType>
            </List>
        </DataType>
        """,
        [(12, ["ab", "c", "de"]), (23, ["abc", "de"])],
    ),
]


def test_conversion_normalizes_xml_string():
    raw_xml_string = """
    <DataType>
        <Basic>String</Basic>
    </DataType>
    """

    python_any = SilaAnyType(raw_xml_string, "abc")
    message = Any().to_message(python_any)

    assert message.type == "<DataType><Basic>String</Basic></DataType>"
    assert Any().to_native_type(message).type_xml == "<DataType><Basic>String</Basic></DataType>"


@pytest.mark.parametrize("test_value", TEST_VALUES, ids=lambda v: v.name)
def test_value_roundtrip(test_value: AnyTestValue):
    # create input
    python_any = SilaAnyType(test_value.type_xml, test_value.python_value)

    any_message = Any().to_message(python_any)
    python_any = Any().to_native_type(any_message)

    assert python_any.value == test_value.python_value


def test_create_void():
    msg = Any().to_message(SilaAnyType.create_void())
    assert Any().to_native_type(msg) == SilaAnyType.create_void()
