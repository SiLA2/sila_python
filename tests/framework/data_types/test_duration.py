from datetime import timedelta
from math import isclose

import pytest

from sila2.framework.command.duration import Duration


@pytest.fixture
def duration_field(silaframework_pb2_module) -> Duration:
    return Duration(silaframework_pb2_module)


def test_to_native_type(duration_field, silaframework_pb2_module):
    msg = silaframework_pb2_module.Duration(seconds=3, nanos=123456789)
    val = duration_field.to_native_type(msg)

    assert isinstance(val, timedelta)
    assert isclose(val.total_seconds(), 3.123457)


def test_to_message(duration_field, silaframework_pb2_module):
    msg = duration_field.to_message(timedelta(seconds=3, microseconds=123456))

    assert isinstance(msg, silaframework_pb2_module.Duration)
    assert msg.seconds == 3
    assert isclose(msg.nanos, 123456000)
