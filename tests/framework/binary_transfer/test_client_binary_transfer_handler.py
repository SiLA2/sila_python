import base64
import datetime
import typing
import unittest.mock
import uuid
from os.path import join

import grpc
import grpc_testing
import pytest
from grpc.framework.foundation import logging_pool

from sila2 import resource_dir
from sila2.client.sila_client import SilaClient
from sila2.framework.binary_transfer.binary_transfer_error import BinaryDownloadFailed, InvalidBinaryTransferUUID
from sila2.framework.binary_transfer.client_binary_transfer_handler import (
    BinaryChunk,
    BinaryInfo,
    ClientBinaryTransferHandler,
)
from sila2.framework.utils import run_protoc

SiLABinaryTransfer_pb2, SiLABinaryTransfer_pb2_grpc = run_protoc(
    join(resource_dir, "proto", "SiLABinaryTransfer.proto")
)
SiLAFramework_pb2, SiLAFramework_pb2_grpc = run_protoc(join(resource_dir, "proto", "SiLAFramework.proto"))


if typing.TYPE_CHECKING:
    from sila2.framework.pb2 import SiLABinaryTransfer_pb2, SiLAFramework_pb2


@pytest.fixture
def client_execution_thread_pool():
    _client_execution_thread_pool = logging_pool.pool(1)
    yield _client_execution_thread_pool
    _client_execution_thread_pool.shutdown(wait=True)


@pytest.fixture
def sila_client():
    sila_client = unittest.mock.Mock(spec=SilaClient)
    sila_client._channel = grpc_testing.channel(
        SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name.values(),
        grpc_testing.strict_real_time(),
    )

    return sila_client


class TestGetBinaryInfo:
    # Get binary info for binary transfer uuid.
    def test_get_binary_info(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary info
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_binary_info,
            binary_transfer_uuid,
        )

        # Assert that the method requests the binary info from the server
        (invocation_metadata, request, rpc) = sila_client._channel.take_unary_unary(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetBinaryInfo"]
        )
        assert request == SiLABinaryTransfer_pb2.GetBinaryInfoRequest(binaryTransferUUID=binary_transfer_uuid)

        # Respond with the binary info
        response = SiLABinaryTransfer_pb2.GetBinaryInfoResponse(
            binarySize=8,
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=10),
        )
        rpc.send_initial_metadata(())
        rpc.terminate(response, (), grpc.StatusCode.OK, "")

        # Assert that the method returns the correct binary info
        binary_info = application_future.result()
        assert binary_info == BinaryInfo(
            binary_size=8,
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
        )

    # Get binary info raises an error on invalid binary transfer uuid.
    def test_get_binary_info_raises_with_invalid_binary_transfer_uuid(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary info
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_binary_info,
            binary_transfer_uuid,
        )

        # Respond with an invalid binary transfer uuid error
        (invocation_metadata, request, rpc) = sila_client._channel.take_unary_unary(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetBinaryInfo"]
        )
        message = base64.standard_b64encode(
            InvalidBinaryTransferUUID("Receieved an unknown Binary Transfer UUID").to_message().SerializeToString()
        ).decode("ascii")

        rpc.send_initial_metadata(())
        rpc.terminate(None, (), grpc.StatusCode.ABORTED, message)

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            InvalidBinaryTransferUUID,
            match=r"Receieved an unknown Binary Transfer UUID",
        ):
            application_future.result()

    # Get binary info raises an error on rpc cancellation.
    def test_get_binary_info_raises_on_cancellation(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary info
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_binary_info,
            binary_transfer_uuid,
        )

        # Cancel rpc
        (invocation_metadata, request, rpc) = sila_client._channel.take_unary_unary(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetBinaryInfo"]
        )
        rpc.terminate(None, (), grpc.StatusCode.CANCELLED, "Unexpected cancellation")

        # Assert that the method raises an binary download failed error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Unexpected cancellation",
        ):
            application_future.result()


class TestGetChunks:
    # Get binary chunks for binary transfer uuid with a small payload.
    def test_get_chunks_with_small_payload(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=5,
            max_chunk_size=8,
        )

        # Assert that the method requests the binary chunk from the server
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        rpc.send_initial_metadata(())
        request = rpc.take_request()
        assert request == SiLABinaryTransfer_pb2.GetChunkRequest(
            binaryTransferUUID=binary_transfer_uuid,
            offset=0,
            length=5,
        )

        # Respond with the binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=0,
            payload=b"01234",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=10),
        )
        rpc.send_response(response)
        rpc.terminate((), grpc.StatusCode.OK, "")

        # Assert that the method returns the correct binary chunks
        chunks = application_future.result()
        assert len(chunks) == 1
        assert chunks[0] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=0,
            payload=b"01234",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
        )

    # Get binary chunks for binary transfer uuid with a large payload.
    def test_get_chunks_with_large_payload(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=20,
            max_chunk_size=8,
        )

        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        rpc.send_initial_metadata(())

        # Assert that the method requests the first binary chunk from the server
        request = rpc.take_request()
        assert request == SiLABinaryTransfer_pb2.GetChunkRequest(
            binaryTransferUUID=binary_transfer_uuid,
            offset=0,
            length=8,
        )

        # Respond with the first binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=0,
            payload=b"01234567",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=10),
        )
        rpc.send_response(response)

        # Assert that the method requests the second binary chunk from the server
        request = rpc.take_request()
        assert request == SiLABinaryTransfer_pb2.GetChunkRequest(
            binaryTransferUUID=binary_transfer_uuid,
            offset=8,
            length=8,
        )

        # Respond with the second binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=8,
            payload=b"89abcdef",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=5),
        )
        rpc.send_response(response)

        # Assert that the method requests the third binary chunk from the server
        request = rpc.take_request()
        assert request == SiLABinaryTransfer_pb2.GetChunkRequest(
            binaryTransferUUID=binary_transfer_uuid,
            offset=16,
            length=4,
        )

        # Respond with the third binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=16,
            payload=b"ghij",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=0),
        )
        rpc.send_response(response)

        rpc.terminate((), grpc.StatusCode.OK, "")

        # Assert that the method returns the correct binary chunks
        chunks = application_future.result()
        assert len(chunks) == 3
        assert chunks[0] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=0,
            payload=b"01234567",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
        )
        assert chunks[1] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=8,
            payload=b"89abcdef",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.005),
        )
        assert chunks[2] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=16,
            payload=b"ghij",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0),
        )

    # Get binary chunks for binary transfer uuid in correct order.
    def test_get_chunks_in_correct_order(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=20,
            max_chunk_size=8,
        )

        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        rpc.send_initial_metadata(())
        rpc.take_request()
        rpc.take_request()
        rpc.take_request()

        # Respond with the third binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=16,
            payload=b"ghij",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=0),
        )
        rpc.send_response(response)

        # Respond with the second binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=8,
            payload=b"89abcdef",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=5),
        )
        rpc.send_response(response)

        # Respond with the first binary chunk
        response = SiLABinaryTransfer_pb2.GetChunkResponse(
            binaryTransferUUID=binary_transfer_uuid,
            offset=0,
            payload=b"01234567",
            lifetimeOfBinary=SiLAFramework_pb2.Duration(seconds=20, nanos=10),
        )
        rpc.send_response(response)

        rpc.terminate((), grpc.StatusCode.OK, "")

        # Assert that the method returns the correct binary chunks
        chunks = application_future.result()
        assert len(chunks) == 3
        assert chunks[0] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=0,
            payload=b"01234567",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
        )
        assert chunks[1] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=8,
            payload=b"89abcdef",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.005),
        )
        assert chunks[2] == BinaryChunk(
            binary_transfer_uuid=binary_transfer_uuid,
            offset=16,
            payload=b"ghij",
            lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0),
        )

    # Get binary chunks raises an error on invalid binary transfer uuid.
    def test_get_chunks_raises_with_invalid_binary_transfer_uuid(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=20,
            max_chunk_size=8,
        )

        # Respond with an invalid binary transfer uuid error
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        response = base64.standard_b64encode(
            InvalidBinaryTransferUUID("Receieved an unknown Binary Transfer UUID").to_message().SerializeToString()
        ).decode("ascii")
        rpc.send_initial_metadata(())
        rpc.terminate((), grpc.StatusCode.ABORTED, response)

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            InvalidBinaryTransferUUID,
            match=r"Receieved an unknown Binary Transfer UUID",
        ):
            application_future.result()

    # Get binary chunks raises an error on failed binary download.
    def test_get_chunks_raises_with_failed_binary_download(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=20,
            max_chunk_size=8,
        )

        # Respond with a failed binary download error
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        response = base64.standard_b64encode(
            BinaryDownloadFailed("Binary download failed for unknown chunk").to_message().SerializeToString()
        ).decode("ascii")
        rpc.send_initial_metadata(())
        rpc.terminate((), grpc.StatusCode.ABORTED, response)

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Binary download failed for unknown chunk",
        ):
            application_future.result()

    # Get binary chunks raises an error on rpc cancellation.
    def test_get_chunks_raises_on_cancellation(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Get binary chunks
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.get_chunks,
            binary_transfer_uuid,
            binary_size=20,
            max_chunk_size=8,
        )

        # Cancel rpc
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["GetChunk"]
        )
        rpc.terminate((), grpc.StatusCode.CANCELLED, "Unexpected cancellation")

        # Assert that the method raises an binary download failed error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Unexpected cancellation",
        ):
            application_future.result()


class TestDeleteBinary:
    # Delete binary for binary transfer uuid.
    def test_delete_binary(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Delete binary
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.delete_binary,
            binary_transfer_uuid,
        )

        # Assert that the method requests to delete the binary from the server
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["DeleteBinary"]
        )
        rpc.send_initial_metadata(())
        request = rpc.take_request()
        assert request == SiLABinaryTransfer_pb2.DeleteBinaryRequest(binaryTransferUUID=binary_transfer_uuid)

        # Respond with the binary deletion
        response = SiLABinaryTransfer_pb2.DeleteBinaryResponse()
        rpc.send_response(response)
        rpc.terminate((), grpc.StatusCode.OK, "")

        # Assert that the method returns the correct binary deletion
        application_future.result()

    # Delete binary raises an error on invalid binary transfer uuid.
    def test_delete_binary_raises_with_invalid_binary_transfer_uuid(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Delete binary
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.delete_binary,
            binary_transfer_uuid,
        )

        # Respond with an invalid binary transfer uuid error
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["DeleteBinary"]
        )
        response = base64.standard_b64encode(
            InvalidBinaryTransferUUID("Receieved an unknown Binary Transfer UUID").to_message().SerializeToString()
        ).decode("ascii")
        rpc.send_initial_metadata(())
        rpc.terminate((), grpc.StatusCode.ABORTED, response)

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            InvalidBinaryTransferUUID,
            match=r"Receieved an unknown Binary Transfer UUID",
        ):
            application_future.result()

    # Delete binary raises an error on rpc cancellation.
    def test_delete_binary_raises_on_cancellation(
        self,
        client_execution_thread_pool: logging_pool._LoggingPool,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Delete binary
        application_future = client_execution_thread_pool.submit(
            client_binary_transfer_handler.delete_binary,
            binary_transfer_uuid,
        )

        # Cancel rpc
        (invocation_metadata, rpc) = sila_client._channel.take_stream_stream(
            SiLABinaryTransfer_pb2.DESCRIPTOR.services_by_name["BinaryDownload"].methods_by_name["DeleteBinary"]
        )
        rpc.terminate((), grpc.StatusCode.CANCELLED, "Unexpected cancellation")

        # Assert that the method raises an binary download failed error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Unexpected cancellation",
        ):
            application_future.result()


class TestComputeChunks:
    @pytest.mark.parametrize(
        ("binary_size", "max_chunk_size", "expected"),
        [
            (4, 8, [(0, 4)]),
            (8, 8, [(0, 8)]),
            (13, 8, [(0, 8), (8, 5)]),
            (16, 8, [(0, 8), (8, 8)]),
            (29, 8, [(0, 8), (8, 8), (16, 8), (24, 5)]),
        ],
    )
    # Delete binary for binary transfer uuid.
    def test_compute_chunks(
        self,
        binary_size: int,
        max_chunk_size: int,
        expected: list,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)

        # Compute chunks
        chunks = client_binary_transfer_handler.compute_chunks(binary_size=binary_size, max_chunk_size=max_chunk_size)

        # Assert that the method returns the correct chunks
        assert list(chunks) == expected


class TestToNativeType:
    # Convert to native type with binary transfer uuid.
    def test_to_native_type(
        self,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)
        client_binary_transfer_handler.get_binary_info = unittest.mock.Mock(
            return_value=BinaryInfo(
                binary_size=20,
                lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
            )
        )
        client_binary_transfer_handler.get_chunks = unittest.mock.Mock(
            return_value=[
                BinaryChunk(
                    binary_transfer_uuid=binary_transfer_uuid,
                    offset=0,
                    payload=b"01234567",
                    lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
                ),
                BinaryChunk(
                    binary_transfer_uuid=binary_transfer_uuid,
                    offset=8,
                    payload=b"89abcdef",
                    lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.005),
                ),
                BinaryChunk(
                    binary_transfer_uuid=binary_transfer_uuid,
                    offset=16,
                    payload=b"ghij",
                    lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0),
                ),
            ]
        )
        client_binary_transfer_handler.delete_binary = unittest.mock.Mock()

        # Convert to native type
        native_type = client_binary_transfer_handler.to_native_type(binary_transfer_uuid)

        # Assert that the method returns the correct native type
        assert native_type == b"0123456789abcdefghij"

    # Convert to native type with a cached binary payload.
    def test_to_native_type_from_cache(
        self,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)
        client_binary_transfer_handler.known_binaries[binary_transfer_uuid] = b"0123456789abcdefghij"

        # Convert to native type
        native_type = client_binary_transfer_handler.to_native_type(binary_transfer_uuid)

        # Assert that the method returns the correct native type
        assert native_type == b"0123456789abcdefghij"

    # Convert to native type raises an error on invalid binary transfer uuid.
    def test_to_native_type_raises_with_invalid_binary_transfer_uuid(
        self,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)
        client_binary_transfer_handler.get_binary_info = unittest.mock.Mock(
            side_effect=InvalidBinaryTransferUUID("Receieved an unknown Binary Transfer UUID")
        )

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            InvalidBinaryTransferUUID,
            match=r"Receieved an unknown Binary Transfer UUID",
        ):
            client_binary_transfer_handler.to_native_type(binary_transfer_uuid)

    # Convert to native type raises an error on failed binary download.
    def test_to_native_type_raises_with_failed_binary_download(
        self,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)
        client_binary_transfer_handler.get_binary_info = unittest.mock.Mock(
            return_value=BinaryInfo(
                binary_size=20,
                lifetime_of_binary=datetime.timedelta(seconds=20, microseconds=0.01),
            )
        )
        client_binary_transfer_handler.get_chunks = unittest.mock.Mock(
            side_effect=BinaryDownloadFailed("Binary download failed for unknown chunk"),
        )

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Binary download failed for unknown chunk",
        ):
            client_binary_transfer_handler.to_native_type(binary_transfer_uuid)

    # Convert to native type raises an error on unexpected error
    def test_to_native_type_raises_with_unexpected_error(
        self,
        sila_client: SilaClient,
    ):
        # Initialize the binary transfer handler
        binary_transfer_uuid = str(uuid.uuid4())
        client_binary_transfer_handler = ClientBinaryTransferHandler(sila_client)
        client_binary_transfer_handler.get_binary_info = unittest.mock.Mock(side_effect=RuntimeError("Socket closed"))

        # Assert that the method raises an invalid binary transfer uuid error
        with pytest.raises(
            BinaryDownloadFailed,
            match=r"Socket closed",
        ):
            client_binary_transfer_handler.to_native_type(binary_transfer_uuid)
