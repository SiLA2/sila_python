from os.path import join

import pytest

from sila2 import resource_dir
from sila2.framework.utils import run_protoc


def test_protoc():
    pb2_module, grpc_module = run_protoc(join(resource_dir, "proto", "SiLABinaryTransfer.proto"))
    assert hasattr(pb2_module, "GetBinaryInfoRequest")
    assert hasattr(grpc_module, "BinaryUploadServicer")

    with pytest.raises(RuntimeError):
        run_protoc("missing-file.proto")

    with pytest.raises(RuntimeError):
        run_protoc(__file__)
