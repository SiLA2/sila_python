import pytest

from sila2.client import SilaClient
from sila2.framework.errors.sila_connection_error import SilaConnectionError
from sila2.server import SilaServer
from tests.utils import generate_port


def test_client_address_properties():
    server = SilaServer("Test", "Test", "Test", "0.1", "https://gitlab.com/sila2/sila_python")
    port = generate_port()
    address = "127.0.0.1"

    try:
        server.start_insecure(address, port, enable_discovery=False)
        client = SilaClient(address, port, insecure=True)

        assert client.address == address
        assert client.port == port

    finally:
        server.stop()


def test_client_close():
    server = SilaServer("Test", "Test", "Test", "0.1", "https://gitlab.com/sila2/sila_python")
    port = generate_port()
    address = "127.0.0.1"

    try:
        server.start_insecure(address, port, enable_discovery=False)
        client = SilaClient(address, port, insecure=True)

        client.SiLAService.ServerName.get()  # verify channel is open

        client.close()

        with pytest.raises(SilaConnectionError, match="closed channel"):
            client.SiLAService.ServerName.get()

    finally:
        server.stop()


def test_client_context_manager():
    server = SilaServer("Test", "Test", "Test", "0.1", "https://gitlab.com/sila2/sila_python")
    port = generate_port()
    address = "127.0.0.1"

    try:
        server.start_insecure(address, port, enable_discovery=False)

        with SilaClient(address, port, insecure=True) as client:
            client.SiLAService.ServerName.get()  # verify channel is open

        with pytest.raises(SilaConnectionError, match="closed channel"):
            client.SiLAService.ServerName.get()

    finally:
        server.stop()
